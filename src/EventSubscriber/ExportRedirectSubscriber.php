<?php

namespace Drupal\views_data_export_tcpdf\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views_data_export_tcpdf\Plugin\views\display\PdfDataExport;
use Drupal\file\Entity\File;

/**
 * Class ExportRedirectSubscriber.
 *
 * @package Drupal\views_data_export_tcpdf
 */
class ExportRedirectSubscriber implements EventSubscriberInterface
{

  /**
   * The current route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The allowed PDF mime types.
   *
   * @var string[]
   */
  protected array $pdfContentTypes;

  /**
   * Constructs an ExportRedirectSubscriber.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match object.
   */
  public function __construct(RouteMatchInterface $current_route_match)
  {
    $this->routeMatch = $current_route_match;
    $this->pdfContentTypes = ['pdf'];
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    // This priority ensures that it happens after the route match was set up
    // and the formats were added into requests, etc.
    return [
      KernelEvents::REQUEST => ['exportRedirect', -64],
    ];
  }

  /**
   * Redirects export if all the necessary route parameters are present.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function exportRedirect(RequestEvent $event): void
  {
    $request = $event->getRequest();
    // Determine if this is a PDF form route.
    $view_id = $this->routeMatch->getParameter('view_id');
    $display_id = $this->routeMatch->getParameter('display_id');
    if (isset($view_id, $display_id) && $this->routeMatch->getRouteName() === "view.$view_id.$display_id.export") {
      $args = $this->getViewArgs($this->routeMatch);
      $pdf_file = $this->routeMatch->getParameter('_pdf_file');
      $pdf_file = File::load($pdf_file);
      if ($pdf_file && in_array($request->getFormat($pdf_file->getMimeType()), $this->pdfContentTypes, true)) {
        $args['_pdf_file'] = $pdf_file;
        $event->setResponse(PdfDataExport::buildResponse($view_id, $display_id, $args));
      }
    }
  }

  /**
   * Get the view arguments from a route.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface|null $route_match
   *   The route match where the arguments are stored.
   *
   * @return array
   *   The view arguments.
   *
   * @see \Drupal\views_data_export_tcpdf\Form\PdfExportForm::getViewArgs
   */
  protected function getViewArgs(?RouteMatchInterface $route_match = NULL): array
  {
    if (!isset($route_match)) {
      return [];
    }

    $route = $route_match->getRouteObject();
    $map = $route->getOption('_view_argument_map') ?? [];
    $args = [];

    foreach ($map as $attribute => $parameter_name) {
      // Allow parameters to be pulled from the request.
      // The map stores the actual name of the parameter in the request. Views
      // which override existing controller, use for example 'node' instead of
      // arg_nid as name.
      $attribute = $map[$attribute] ?? $attribute;
      $arg = $route_match->getRawParameter($attribute) ?? $route_match->getParameter($attribute);

      if (isset($arg)) {
        $args[] = $arg;
      }
    }

    return $args;
  }
}
