<?php

namespace Drupal\views_data_export_tcpdf\Form;

use Drupal\Component\Utility\Environment;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views_data_export_tcpdf\Plugin\views\display\PdfDataExport;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\file\Entity\File;

/**
 * Class PdfExportForm.
 *
 * @package Drupal\views_data_export_tcpdf\Form
 */
class PdfExportForm extends FormBase
{

  /**
   * Uploaded file entity.
   *
   * @var \Drupal\file\Entity\File|null
   */
  protected ?File $file = null;

  /**
   * The allowed pdf mime types.
   *
   * @var string[]
   */
  protected array $pdfMimeTypes = [
    'application/vnd.pdf',
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'pdf_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $view_id = NULL, $display_id = NULL, RouteMatchInterface $route_match = NULL): array
  {
    $pdf_file = $route_match->getParameter('_pdf_file');
    $pdf_file = File::load($pdf_file);
    if ($pdf_file && in_array($pdf_file->getMimeType(), $this->pdfMimeTypes, true)) {
      $this->file = $pdf_file;
    }

    $form['export_external'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export to existing PDF'),
      '#default_value' => TRUE,
      '#weight' => -1,
    ];

    $validators = [
      'file_validate_extensions' => ['pdf pdfx'],
      'file_validate_size' => [Environment::getUploadMaxSize()],
    ];

    if ($pdf_file == '-1' || is_null($this->file)) {
      $form['pdf_file'] = [
        '#type' => 'file',
        '#title' => $this->t('PDF File'),
        '#description' => [
          '#theme' => 'file_upload_help',
          '#description' => $this->t('An pdf file'),
          '#upload_validators' => $validators,
        ],
        '#upload_validators' => $validators,
      ];
    } else {
      $form['pdf_file'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'file',
        '#default_value' => $this->file,
        '#disabled' => TRUE,
      ];
    }

    $form['pdf_file']['#states']['visible'][':input[name="export_external"]']['checked'] = TRUE;
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Get Pdf File'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void
  {
    parent::validateForm($form, $form_state);
    $export_external = $form_state->getValue('export_external');

    if ($export_external) {
      if (!$this->file) {
        $this->file = file_save_upload('pdf_file', $form['pdf_file']['#upload_validators'], FALSE, 0);
        // Ensure we have the file uploaded.
        if (!$this->file) {
          $form_state->setErrorByName('pdf_file', $this->t('PDF File not found.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    [$view_id, $display_id, $route_match] = $form_state->getBuildInfo()['args'];
    $args = $this->getViewArgs($route_match);

    $export_external = $form_state->getValue('export_external');
    if ($export_external) {
      // Nothing to do here but pass off as data export.
      $args['_pdf_file'] = $this->file;
      // When setting the response, the form isn't rebuilt or anything.
      // And since the file is downloaded (usually)
      // the form remains on the screen which is bad for UX.
      // reloading the tab will tell you to leave but that is not helpful.
    }
    $form_state->setResponse(PdfDataExport::buildResponse($view_id, $display_id, $args));
    // Need to somehow re-redirect to somewhere else.
  }

  /**
   * Get the view arguments from a route.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface|null $route_match
   *   The route match where the arguments are stored.
   *
   * @return array
   *   The view arguments.
   *
   * @see Drupal\views\Routing\ViewPageController::handle
   */
  protected function getViewArgs(?RouteMatchInterface $route_match = NULL): array
  {
    if (!isset($route_match)) {
      return [];
    }

    $route = $route_match->getRouteObject();
    $map = $route->getOption('_view_argument_map') ?? [];
    $args = [];

    foreach ($map as $attribute => $parameter_name) {
      // Allow parameters to be pulled from the request.
      // The map stores the actual name of the parameter in the request. Views
      // which override existing controller, use for example 'node' instead of
      // arg_nid as name.
      $attribute = $map[$attribute] ?? $attribute;
      $arg = $route_match->getRawParameter($attribute) ?? $route_match->getParameter($attribute);

      if (isset($arg)) {
        $args[] = $arg;
      }
    }

    return $args;
  }
}
