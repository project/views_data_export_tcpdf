<?php

namespace Drupal\views_data_export_tcpdf\Plugin\views\display;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\views_data_export\Plugin\views\display\DataExport;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alters an existing file for PDF data export.
 *
 * This class is part of the custom module for exporting view data to PDF using
 * TCPDF.
 * It is located in the specified file path and is relevant to altering an
 * existing file.
 */
class PdfDataExport extends DataExport
{

  /**
   * The display plugin that this class is applicable for.
   *
   * @var string
   */
  const DISPLAY_PLUGIN = 'data_export';

  /**
   * {@inheritdoc}
   */
  public function collectRoutes(RouteCollection $collection): void
  {
    parent::collectRoutes($collection);

    $view_id = $this->view->storage->id();
    $display_id = $this->display['id'];

    if ($route = $collection->get("view.$view_id.$display_id")) {
      if ($this->getContentType() === 'pdf') {
        $export_route = $this->getExportRoute($view_id, $display_id);
        if ($this->getOption('flip_path')) {
          $export_route->setPath($route->getPath() . '/{_pdf_file}');
          $route->setPath($route->getPath() . '/result');

          $collection->remove("view.$view_id.$display_id");
          $collection->add("view.$view_id.$display_id.result", $route);
        }
        $collection->add("view.$view_id.$display_id.export", $export_route);
      }
    }
  }

  /**
   * Generates a route entry for a given view and display.
   *
   * @param string $view_id
   *   The ID of the view.
   * @param string $display_id
   *   The current display ID.
   *
   * @return \Symfony\Component\Routing\Route
   *   The route for the view.
   */
  protected function getExportRoute(string $view_id, string $display_id): Route
  {
    $defaults = [
      '_form' => 'Drupal\views_data_export_tcpdf\Form\PdfExportForm',
      '_title' => "Choose Export Location",
      'view_id' => $view_id,
      '_pdf_file' => $this->getOption('default_fid') ?: -1,
    ];

    $bits = explode('/', $this->getOption('path'));
    $arg_counter = 0;
    $argument_ids = array_keys((array) $this->getOption('arguments'));
    $total_arguments = count($argument_ids);
    $argument_map = [];

    foreach ($bits as $pos => $bit) {
      if ($bit === '%') {
        $arg_id = 'arg_' . $arg_counter++;
        $bits[$pos] = '{' . $arg_id . '}';
        $argument_map[$arg_id] = $arg_id;
      } elseif (strpos($bit, '%') === 0) {
        $parameter_name = substr($bit, 1);
        $arg_id = 'arg_' . $arg_counter++;
        $argument_map[$arg_id] = $parameter_name;
        $bits[$pos] = '{' . $parameter_name . '}';
      }
    }

    while (($total_arguments - $arg_counter) > 0) {
      $arg_id = 'arg_' . $arg_counter++;
      $bits[] = '{' . $arg_id . '}';
      $defaults[$arg_id] = NULL;
      $argument_map[$arg_id] = $arg_id;
    }

    $bits[] = 'export';
    $bits[] = '{_pdf_file}';

    if ($this->isDefaultTabPath()) {
      $bit = array_pop($bits);
      if (empty($bits)) {
        $bits[] = $bit;
      }
    }

    $route_path = '/' . implode('/', $bits);
    return new Route($route_path, $defaults);
  }

  /**
   * {@inheritdoc}
   */
  public static function buildResponse(string $view_id, string $display_id, array $args = [], &$view = []): CacheableResponse
  {
    $original = $args['_pdf_file'] ?? NULL;
    $original_response = parent::buildResponse($view_id, $display_id, $args);
    $build = static::buildBasicRenderable($view_id, $display_id, $args);

    if ((Request::createFromGlobals()->getFormat($original_response->headers->get('Content-type'))) === 'pdf') {
      $response = new CacheableResponse('', 200);
      $build['#response'] = $response;

      $renderer = \Drupal::service('renderer');
      $file_system = \Drupal::service('file_system');

      $serialized = $renderer->renderRoot($build);

      if ($original) {
        try {
          return $response;
        } catch (\Exception $e) {
          \Drupal::logger('views_data_export_tcpdf')->error('Could not export as pdf file because: ' . $e->getMessage() . ' ' . $e->getTraceAsString());
          return $original_response;
        }
      }
    }
    return $original_response;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void
  {
    parent::buildOptionsForm($form, $form_state);

    if ($form_state->get('section') === 'path') {
      $form['#title'] .= $this->t('The menu path or URL of this view');
      $form['path']['#description'] .= $this->t('. Navigate to the above path plus /export/%pdf_file to provide extra information just before downloading. pdf_file are optional url parameters');

      $form['flip_path'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Switch the Export Route'),
        '#description' => $this->t('Make the Export route the normal route (with the same route parameters). The old result route will be kept at the set path plus /result'),
        '#default_value' => $this->getOption('flip_path') ?: FALSE,
      ];

      $form['default_fid'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'file',
        '#title' => $this->t('Default File ID'),
        '#description' => $this->t("The Default file id to use if one isn't provided. Leave blank to upload a file each time."),
        '#default_value' => $this->getOption('default_fid') ? File::load($this->getOption('default_fid')) : '',
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state): void
  {
    parent::validateOptionsForm($form, $form_state);

    if ($form_state->get('section') === 'path') {
      $fid = $form_state->getValue('default_fid');
      if ($fid && $file = File::load($fid)) {
        if (!in_array($file->getMimeType(), ['application/pdf'], true)) {
          $form_state->setErrorByName('default_fid', $this->t('This file entity must point to a PDF file.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state): void
  {
    parent::submitOptionsForm($form, $form_state);

    if ($form_state->get('section') === 'path') {
      $this->setOption('default_fid', $form_state->getValue('default_fid'));
      $this->setOption('flip_path', (bool) $form_state->getValue('flip_path'));
    }
  }
}
