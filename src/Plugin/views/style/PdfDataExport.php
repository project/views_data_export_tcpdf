<?php

namespace Drupal\views_data_export_tcpdf\Plugin\views\style;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views_data_export\Plugin\views\style\DataExport;

/**
 * A style plugin for Pdf export views.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "pdf_export",
 *   title = @Translation("Pdf export"),
 *   help = @Translation("Configurable row output for Pdf exports."),
 *   display_types = {"data"}
 * )
 */
class PdfDataExport extends DataExport
{

  /**
   * Constructs a Plugin object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $serializer
   *   The serializer for the plugin instance.
   * @param array $serializer_formats
   *   The serializer formats for the plugin instance.
   * @param array $serializer_format_providers
   *   The serializer format providers for the plugin instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $serializer, array $serializer_formats, array $serializer_format_providers)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer, $serializer_formats, $serializer_format_providers);

    $this->formats = ['PDF'];
    $this->formatProviders = ['pdf' => 'pdf_data_export'];
  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions(): array
  {
    $options = parent::defineOptions();
    $options['pdf_settings']['contains'] = [
      'pdf_format' => ['default' => 'PDF'],
    ];
    $options['pdf_settings']['metadata']['contains'] = [
      'creator' => ['default' => ''],
      'last_modified_by' => ['default' => ''],
      'title' => ['default' => ''],
      'description' => ['default' => ''],
      'subject' => ['default' => ''],
      'keywords' => ['default' => ''],
      'category' => ['default' => ''],
      'manager' => ['default' => ''],
      'company' => ['default' => ''],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void
  {
    parent::buildOptionsForm($form, $form_state);

    if ($form_state->get('section') === 'style_options') {
      // Change format to radios instead, since multiple formats here do not
      // make sense as they do for REST exports.
      $form['formats']['#type'] = 'radios';
      $form['formats']['#default_value'] = reset($this->options['formats']);

      // Remove now confusing description.
      unset($form['formats']['#description']);

      // PDF options.
      $pdf_options = $this->options['pdf_settings'];
      $form['pdf_settings'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('PDF settings'),
        '#tree' => TRUE,
      ];

      $metadata = !empty($pdf_options['metadata']) ? array_filter($pdf_options['metadata']) : [];

      // PDF metadata.
      $form['pdf_settings']['metadata'] = [
        '#type' => 'details',
        '#title' => $this->t('Document metadata'),
        '#open' => !empty($metadata),
      ];

      $pdf_fields = [
        'creator' => $this->t('Author/creator name'),
        'last_modified_by' => $this->t('Last modified by'),
        'title' => $this->t('Title'),
        'description' => $this->t('Description'),
        'subject' => $this->t('Subject'),
        'keywords' => $this->t('Keywords'),
        'category' => $this->t('Category'),
        'manager' => $this->t('Manager'),
        'company' => $this->t('Company'),
      ];

      foreach ($pdf_fields as $pdf_field_key => $pdf_field_title) {
        $form['pdf_settings']['metadata'][$pdf_field_key] = [
          '#type' => 'textfield',
          '#title' => $pdf_field_title,
        ];

        if (isset($pdf_options['metadata'][$pdf_field_key])) {
          $form['pdf_settings']['metadata'][$pdf_field_key]['#default_value'] = $pdf_options['metadata'][$pdf_field_key];
        }
      }
    }
  }

  /**
   * Attaches the PDF export link to the view.
   *
   * @param array $build
   *   The render array of the view.
   * @param \Drupal\views\Plugin\views\display\DisplayPluginBase $display
   *   The display plugin of the view.
   * @param \Drupal\Core\Url $url
   *   The URL object for the export link.
   * @param string|null $title
   *   The title of the export link.
   * @param array $options
   *   Additional options for the link.
   */
  public function attachTo(array &$build, $display_id, Url $url, $title)
  {

    // @todo This mostly hard-codes PDF handling. Figure out how to abstract.
    $url_options = [];
    $input = $this->view->getExposedInput();
    if ($input) {
      $url_options['query'] = $input;
    }
    if ($pager = $this->view->getPager()) {
      $url_options['query']['page'] = $pager->getCurrentPage();
    }

    if (!empty($this->options['formats'])) {
      $url_options['query']['_format'] = reset($this->options['formats']);
    }
    $url->setOptions($url_options);
    $url = $url->toString();

    // Add the PDF icon to the view.
    $type = $this->displayHandler->getContentType();

    $this->view->feedIcons[] = [
      '#theme' => 'export_icon',
      '#url' => $url,
      '#type' => mb_strtoupper($type),
      '#theme_wrappers' => [
        'container' => [
          '#attributes' => [
            'class' => [
              Html::cleanCssIdentifier($type) . '-feed',
              'views-data-export-feed',
            ],
          ],
        ],
      ],
      '#attached' => [
        'library' => [
          'views_data_export_tcpdf/views_data_export_tcpdf',
        ],
      ],
    ];
    // Attach a link to the PDF feed, which is an alternate representation.
    $build['#attached']['html_head_link'][][] = [
      'rel' => 'alternate',
      'type' => $this->displayHandler->getMimeType(),
      'title' => $title,
      'href' => $url,
    ];
  }
}
