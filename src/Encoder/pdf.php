<?php

namespace Drupal\views_data_export_tcpdf\Encoder;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Encoder\NormalizationAwareInterface;
use TCPDF;

/**
 * Adds PDF encoder support for the Serialization API.
 */
class Pdf implements EncoderInterface, NormalizationAwareInterface
{

  /**
   * The format that this encoder supports.
   *
   * @var string
   */
  protected static string $format = 'pdf';

  /**
   * Format to write PDF files as.
   *
   * @var string
   */
  protected string $pdfFormat = 'pdf';

  /**
   * {@inheritdoc}
   */
  public function encode($data, $format, array $context = []): string
  {
    $data = match (gettype($data)) {
      'array' => $data,
      'object' => (array) $data,
      default => [$data],
    };

    try {
      $fileName = 'export.pdf';
      $filePath = '/tmp/pdf/export/';
      $fileUri = $filePath . $fileName;

      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // Set document information.
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Drupal');
      $pdf->SetTitle('Drupal');
      $pdf->SetSubject('Drupal');

      // Remove default header/footer.
      $pdf->setPrintHeader(false);
      $pdf->setPrintFooter(false);

      // Set default monospaced font.
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

      // Set margins.
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);

      // Set auto page breaks.
      $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

      // Set image scale factor.
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // Set font.
      $pdf->SetFont('dejavusans', '', 14, '', true);

      // Add a page.
      $pdf->AddPage('L');

      if (isset($context['views_style_plugin'])) {
        if (isset($context['views_style_plugin']->options['pdf_settings'])) {
          $context['views_style_plugin']->options['pdf_settings'];
        }
      }

      if (!empty($context['views_style_plugin']->view)) {
        /** @var \Drupal\views\ViewExecutable $view */
        $view = $context['views_style_plugin']->view;
        if (!empty($view->getTitle())) {
          $pdf->SetFont('times', 'B', 20);
          $pdf->Text(15, 15, $view->getTitle(), 0, false, true, 0, 0, 'C');
          $logo = 'themes/custom/patients/media/img/logo.jpg';
          $pdf->Image($logo, 20, 10, 20, 20, 'jpg', null, 'C');
        }

        // Print the title.
        $pdf->SetFont('dejavusans', 'B', 8, '', true);
        $headers = $this->extractHeaders($data, $context);
        $y = 10;
        foreach ($headers as $header) {
          $pdf->Text($y, 45, $header);
          $y += 30;

          if ($header === 'Documentos') {
            $y += 30;
          }
        }

        $x = 50;
        $pdf->SetFont('dejavusans', 'N', 8, '', true);
        $rows_count = 1;
        foreach ($data as $row) {
          $y = 10;
          foreach ($row as $key => $value) {
            $pdf->Text($y, $x, $value);
            $y += 30;

            if ($key === 'field_documents') {
              $y += 30;
            }
          }
          $x += 5;

          if ($rows_count >= 30) {
            $pdf->AddPage('L');
            $rows_count = 1;
          } else {
            $rows_count++;
          }
        }
      }

      // Ensure file path exists.
      $filesystem = new Filesystem();
      $filesystem->mkdir($filePath);

      // Close and output PDF document.
      $pdf->Output($fileUri, 'F');

      $response = new BinaryFileResponse($fileUri);
      $response->setContentDisposition(
        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
        $fileName
      );
      $response->headers->set('Content-Type', 'application/pdf');

      return $response;
    } catch (\Exception $e) {
      throw new InvalidDataTypeException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function supportsEncoding($format): bool
  {
    return $format === static::$format;
  }

  /**
   * Extract the headers from the data array.
   *
   * @param array $data
   *   The data array.
   * @param array $context
   *   The context options array.
   *
   * @return string[]
   *   An array of headers to be used.
   */
  protected function extractHeaders(array $data, array $context): array
  {
    $headers = [];
    if ($first_row = reset($data)) {
      if (isset($context['header'])) {
        $headers = $context['header'];
      } elseif (isset($context['views_style_plugin'])) {
        /** @var \Drupal\views\ViewExecutable $view */
        $view = $context['views_style_plugin']->view;
        $fields = $view->field;
        foreach ($first_row as $key => $value) {
          $headers[] = !empty($fields[$key]->options['label']) ? $fields[$key]->options['label'] : $key;
        }
      } else {
        $headers = array_keys($first_row);
      }
    }

    return $headers;
  }
}
