# Views Data Export TCPDF

## Overview
The Views Data Export TCPDF module allows you to export data from Views in Drupal to PDF format using the TCPDF library.

## Requirements
- Drupal 8 or higher
- Views module
- TCPDF library

## Installation
1. Download and install the Views Data Export TCPDF module as you would normally install a contributed Drupal module. For more information, see [Installing contributed modules](https://www.drupal.org/docs/extending-drupal/installing-contributed-modules).
2. Download the TCPDF library from [TCPDF](http://www.tcpdf.org/) and place it in the `libraries` folder of your Drupal installation.

## Configuration
1. Navigate to `admin/config/content/views_data_export_tcpdf` to configure the module settings.
2. Set the path to the TCPDF library if it is not automatically detected.

## Usage
1. Create or edit a View.
2. Add a new display of type "Data Export".
3. Select "PDF" as the export format.
4. Configure the fields and settings as needed.
5. Save the View and test the export functionality.

## Maintainers
- Emiliano (emiliano.markisich@gmail.com)

## License
This project is licensed under the GNU General Public License, version 2 or later.

## Credits
This module is maintained by the Drupal community. Special thanks to the contributors and maintainers of the TCPDF library.
